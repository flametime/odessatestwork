
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.UI;

public class CardHolder : MonoBehaviour
{
    public static CardHolder Instance => _instance;
    public Card Card_1 => _firstCard;
    public Card Card_2 => _secondCard;


    [SerializeField] private List <Texture2D> DownloadedImages = new List <Texture2D>();
    [SerializeField] private List <Card> _cards = new List <Card>();

    [SerializeField] private Text _paredCardsCountText;

    private int _firstImage = 0;
    private int _secondImage = 0;
    private int _thirdImage = 0;
    private int _paredCardsCount;

    private Card _firstCard;
    private Card _secondCard;
 
    private  int _card1 = -1;
    private  int _card2 = -1;

    private static CardHolder _instance;

    private void Awake() 
    {
        _instance = this;

        GetJSONS();
    } 

    public void PareCards()
    {
        if(_card1 >=0 && _card2 >= 0)
        {
            if(_card1 != _card2)
            {
                Debug.Log("Nope, lose!");

                _card1 = -1;
                _card2 = -1;

                _firstCard.PlayAnimDelay(1);
                _secondCard.PlayAnimDelay(1);
            }
            else
            {
                Debug.Log("Yep, win!");

                _card1 = -1;
                _card2 = -1;

                _firstCard.DisableCardDelay(1);
                _secondCard.DisableCardDelay(1);

                _cards.Remove(_firstCard);
                _cards.Remove(_secondCard);

                _paredCardsCount++;
                _paredCardsCountText.text = _paredCardsCount.ToString();
                
                if(_cards.Count <= 0)
                {
                    StartCoroutine(Restart());
                }
            }
        }
    }

    IEnumerator Restart()
    {
        yield return new WaitForSeconds(2);
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void SetCard(int cardIndex, Card card)
    {
        if(_card1 >= 0)
        {
            _card2 = cardIndex;
            _secondCard = card;
            PareCards();
        }
        else
        {
            _card1 = cardIndex;
            _firstCard = card;
        }
    }

#region INITIALIZE
    public void GetJSONS()
    {
        StartCoroutine(GetRequest("https://drive.google.com/uc?export=download&id=1OZQWFVWdWxjGPkhAAsYhPU63ydqs3Bvj", (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            } 
            else
            {
                Post post = JsonConvert.DeserializeObject<Post>(req.downloadHandler.text);

                StartCoroutine(DownloadImage(post.linkToImage1));
                StartCoroutine(DownloadImage(post.linkToImage2));
                StartCoroutine(DownloadImage(post.linkToImage3, SetImage)); 
            }
        }));
    }
    IEnumerator GetRequest(string url, System.Action<UnityWebRequest> callback)
    {
        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            yield return request.SendWebRequest();
            callback(request);
        }
    }
    IEnumerator DownloadImage(string MediaUrl, System.Action<int> callback = null)
    {   
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if(request.isNetworkError || request.isHttpError) 
            Debug.Log(request.error);
        else
        {
            //img.texture = ((DownloadHandlerTexture) request.downloadHandler).texture;
            DownloadedImages.Add(((DownloadHandlerTexture) request.downloadHandler).texture);
            //if(callback != null)
                callback?.Invoke(1);
        }
    } 
    public bool ChoseImage(int number)
    {
        switch(number)
        {
            case 0:
            if(_firstImage < 2)
            {
                _firstImage++;
                return true;
            }
            else
            {
                return false;
            }
            case 1:
            if(_secondImage < 2)
            {
                _secondImage++;
                return true;
            }
            else
            {
                return false;
            }

            case 2:
            if(_thirdImage < 2)
            {
                _thirdImage++;
                return true;
            }
            else
            {
                return false;
            }
        }

        return false;
    }
    public void SetImage(int some)
    {
        StartCoroutine(SetImageRoutine());
    }
    private IEnumerator SetImageRoutine()
    {
        while(_firstImage + _secondImage + _thirdImage < 6)
        {
            for (int i = 0; i < _cards.Count; i++)
            {
                var random = Random.Range(0, DownloadedImages.Count);

                if(!_cards[i].HaveImage)
                {
                    if(ChoseImage(random))
                    {
                        _cards[i].SetCard(random, DownloadedImages[random]);
                        Debug.Log("Yep " + random + " " + _cards[i].name + " " );
                        Debug.Log(_firstImage + " " + _secondImage + " " + _thirdImage);
                    }
                    else
                    {
                        Debug.Log("Nope " + random + " " + _cards[i].name);
                        Debug.Log(_firstImage + " " + _secondImage + " " + _thirdImage);
                    }
                }
            }

            yield return new WaitForSeconds(0.2f);   
        }  

        for (int i = 0; i < _cards.Count; i++)
        {
            _cards[i].PlayAnim();
        }    

        yield return new WaitForSeconds(5); 

        for (int i = 0; i < _cards.Count; i++)
        {
            _cards[i].PlayAnim();
            _cards[i].SetOnClick();
        }      
    }
#endregion 

    

}

[System.Serializable]
public class Post
{
    public string linkToImage1;
    public string linkToImage2;
    public string linkToImage3;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

public class Card : MonoBehaviour, IPointerDownHandler
{
    public bool HaveImage => _haveImage;

    [SerializeField] private Animator _animator;
    [SerializeField] private Texture2D[] _sprite;
    [SerializeField] private RawImage _image;
    private bool _haveImage = false;
    private int _index;
    private int _currentImage = 0;


    public UnityEvent onClick = new UnityEvent();


    public void SetCard(int index, Texture2D image)
    {
        if(_haveImage)
            return;


        _index = index;
        _sprite[1] = image;
        _haveImage = true;
    }
    
    public void OnPointerDown(PointerEventData eventData)
    {
        onClick?.Invoke();
    }

    public void ChangeImage()
    {
        switch(_currentImage)
        {
            case 0:
            _currentImage = 1;
            _image.texture = _sprite[1];
            break;

            case 1:
            _currentImage = 0;
            _image.texture = _sprite[0];
            break;
        }
    }

    public void SetOnClick()
    {
        onClick.AddListener(() => OnClick());
    }

    public void OnClick()
    {
        if((CardHolder.Instance.Card_1 != this) && (CardHolder.Instance.Card_1 != this))
        {
            CardHolder.Instance.SetCard(_index, this);
            PlayAnim();
        }
    }

    public void PlayAnim()
    {
        _animator.SetTrigger("Open");
    }

    public void PlayAnimDelay(int delay)
    {
        StartCoroutine(CloseAnimRoutine(delay));
    }

    public void DisableCardDelay(int delay)
    {
        StartCoroutine(DestroyParedCardRoutine(delay));
    }

    IEnumerator DestroyParedCardRoutine(int delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(this.gameObject);
    }

    IEnumerator CloseAnimRoutine(int delay)
    {
        yield return new WaitForSeconds(delay);
        _animator.SetTrigger("Open");
    }

}